import time
from django.shortcuts import render
from .models import Device

def show_people(req):
    macs = [mac.rstrip('\n') for mac in open('/shared/macs.txt')]
    devices = Device.objects.all().order_by('-active')
    devices.update(active=False)
    for mac in macs:
        device, created = Device.objects.get_or_create(mac=mac)
        if device:
            device.active = True
            device.save()
    time.sleep(5)
    return render(req, 'index.html', {'devices': devices})
