from django.db import models

# Create your models here.


class Device(models.Model):

    name = models.CharField(max_length=100, default='Unknow')
    mac = models.CharField(max_length=30)
    active = models.BooleanField(default=False)
    
    def __str__(self):
        return str(self.name) + ' [ ' + str(self.mac) + ' ] '
