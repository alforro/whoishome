# whoishome

Web system to check the devices connected on a network

* create a shared folder

```
sudo mkdir /shared
sudo chmod a+w /shared
```

* set a cron job for every 5 minutes

```
sudo crontab -e
*/5 * * * * nmap -sP -n 192.168.0.0/24 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}' > /shared/macs.txt
```

* install dependencies, initialize database and create superuser

```
cd whoishome
virtuanenv -p python3 venv
source venv/bin/activate
pip install -r requirements.pip
python manage.py migrate
python manage.py createsuperuser
```

